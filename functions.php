<?php

function child_styles() {
	wp_enqueue_style( 'my-child-theme-style', get_stylesheet_directory_uri() . '/style.css', array( 'front-all' ), false, 'all' );
}
add_action( 'wp_enqueue_scripts', 'child_styles', 11 );

// Deshabilitar Gutenberg editor para Wordpress
add_filter('use_block_editor_for_post', '__return_false');
